FROM paulbouwer/hello-kubernetes:1.8

ARG img_version

COPY server.js /usr/src/app/server.js
COPY home.handlebars /usr/src/app/views/home.handlebars

ENV IMGVERSION=$img_version
